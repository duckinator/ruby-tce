require 'tce/version'

module TCE
  TailCall = Module.new

  def self.included(base)
    base.instance_eval do
      def self.method_added(name)
        @@__added_methods__ ||= {}

        return if @@__added_methods__[name]

        super(name)

        @@__added_methods__[name] = _method = self.instance_method(name)
        file, line = _method.source_location

        self.class_eval(<<-EOF, file, line)
          def #{name.to_s}(*args)
            def #{name.to_s}(*args)
              [TCE::TailCall, args]
            end

            ret = [TCE::TailCall, args]
            _method = @@__added_methods__[#{name.inspect}].bind(self)

            while ret.is_a?(Array) && ret.first == TCE::TailCall
              ret = _method.call(*ret[1])
            end

            ret
          end
        EOF
      end
    end
  end
end

class Module
  # Make TCE usable inside of modules and classes.
  include TCE
end

# Make TCE usable in the global scope.
include TCE

