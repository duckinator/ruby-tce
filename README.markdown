# TCE

Adds tail call elimination for Ruby.

For some trivial cases (factorials), recursive implementations using
this library appear to perform better than the most optimized version
I could come up with (using the `redo` keyword instead of a loop).

In all cases, it should at least match performance of iterative
equivalents — after all, [it does use a loop internally](https://gitlab.com/duckinator/ruby-tce/blob/main/lib/tce.rb#L27-29).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tce'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tce

## Usage

```ruby
require 'tce'

def factorial(n, acc = 1)
  return acc if n.zero?

  factorial(n - 1, acc * n)
end

factorial(5) #=> 120
```

## Limitations

When using this gem, TCE cannot be used inside of anonymous functions.

Printing the result of a recursive call yields unexpected results. This
is an implementation detail, and should be hidden, but I have not found
a way to do so without worsening performance.

```ruby
require 'tce'

def countdown(n)
  return n if n.zero?

  p countdown(n - 1)
end

p countdown(1)
```

prints:

```
[TCE::TailCall, [0]]
0
```

## Contributing

1. Fork it ( https://gitlab.com/duckinator/ruby-tce/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
