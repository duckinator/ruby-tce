#!/usr/bin/env ruby

$: << File.join(File.dirname(__FILE__), 'lib')

require 'tce'

class Thing
  # We use `(acc * n) % FACTORIAL_MODULO` instead of `acc * n`.
  # This allows us to test the tail recursion functionality without accumulating
  # excessively large numbers.
  #
  # See:
  #  https://twitter.com/vex0rian/status/531462536483598336
  #  https://twitter.com/vex0rian/status/531462626170380288
  #  https://twitter.com/vex0rian/status/531462987845210112
  #  https://twitter.com/vex0rian/status/531463172356841472
  FACTORIAL_MODULO = 1_000_000_007

  def factorial(n, acc = 1)
    return acc if n.zero?

    factorial(n - 1, (acc * n)  % FACTORIAL_MODULO)
  end
end

thing = Thing.new

p thing.factorial(100_000)        #=> 457992974

